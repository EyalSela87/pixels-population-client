import React from 'react';

import { Row, Col } from 'react-bootstrap';

const Messages = (props) =>
{

    const messages = props.messages.map((m, index) =>
    {
        const even = index % 2 === 0;
        const style = { backgroundColor: even ? "#F0FFFF" : "#F5F5DC" }

        const lines = m.map((line, lineIndex) =>
        {
            return (
                < Col key={'message-' + index + '-line-' + lineIndex} xs="12" >
                    {line}
                </Col >
            )
        })

        return (
            <Col key={'message-' + index} xs="12" className="border rounded my-2 p-2" style={style}>
                <Row>
                    {lines}
                </Row>
            </Col>
        )
    })

    return (
        <React.Fragment>
            <Row>
                <Col xs="12" className="mb-3">Reports:</Col>
            </Row>

            <Row>{messages}</Row>
        </React.Fragment>
    )
}


export default Messages;