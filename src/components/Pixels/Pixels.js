import React from 'react';
import { Row, Col } from 'react-bootstrap';

import './Pixels.css';

const Pixels = (props) =>
{

    const pixels = props.pixels.map(p =>
    {
        const parentA = p.id === 10000000 || p.id === 10000001 ? p : props.pixels.find(pix => p.parentA_id === pix.id);
        const parentB = p.id === 10000000 || p.id === 10000001 ? p : props.pixels.find(pix => p.parentB_id === pix.id);

        const styles = {
            main: { backgroundColor: `rgb(${p.gene.r}, ${p.gene.g}, ${p.gene.b})` },
            parentA: { backgroundColor: `rgb(${parentA.gene.r}, ${parentA.gene.g}, ${parentA.gene.b})` },
            parentB: { backgroundColor: `rgb(${parentB.gene.r}, ${parentB.gene.g}, ${parentB.gene.b})` }
        }

        return (
            <Col key={p.id} xs="3" sm="2" xl="1" className=" mb-3">
                <div className="single-pixel p-2" style={styles.main}>
                    <div className="inner-holder">
                        <div className="small-pixel" style={styles.parentA}></div>
                        <div className="">+</div>
                        <div className="small-pixel" style={styles.parentB}></div>
                    </div>
                </div>
            </Col>
        )
    })

    return (
        <React.Fragment>
            <Row className="Pixels  border rounded">
                <Col xs="12 mb-4 p-3">
                    pixels population:
                </Col>

                <Col xs="12" >
                    <Row className="pixel-holder">{pixels}</Row>
                </Col>
            </Row>


        </React.Fragment>
    )
}


export default Pixels;