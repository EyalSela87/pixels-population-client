import React, { Component } from 'react';
import Axios from 'axios';
import openSocket from 'socket.io-client/dist/socket.io.js';

import { Row, Col, Container } from 'react-bootstrap';

import Messages from '../../components/Messages/Messages';
import Pixels from '../../components/Pixels/Pixels';


import './Main.css';


class Main extends Component
{
    BASE_URL = "http://localhost:4000";
    // BASE_URL = "https://pixes-population.herokuapp.com";

    state = {
        pixels: [],
        messages: []
    }

    componentDidMount()
    {
        const socket = openSocket(this.BASE_URL);
        socket.on('populationChanged', data =>
        {
            const reports = data.data.reports;
            const pixel = {
                id: data.data.newPixel.id,
                gene: { r: data.data.newPixel.r, g: data.data.newPixel.g, b: data.data.newPixel.b },
                parentA_id: data.data.newPixel.parent_A_id,
                parentB_id: data.data.newPixel.parent_B_id
            }

            this.addNewPixel(pixel, reports);
        })

        this.getAllPixels();
    }

    addNewPixel = (pixel, report) =>
    {
        const pixels = [...this.state.pixels];
        const messages = [...this.state.messages];

        pixels.push(pixel);
        messages.push(report)

        this.setState({ pixels: pixels, messages: messages });
        console.log("pixels:", pixels);
    }

    getAllPixels = () =>
    {
        console.log("get all pixels");
        Axios.get(this.BASE_URL + '/getPixels')
            .then(result =>
            {
                const pixels = result.data.data.map(d => { return { id: d.id, gene: { r: d.r, g: d.g, b: d.b }, parentA_id: d.parentA_id, parentB_id: d.parentB_id } });
                console.log("pixels", pixels);
                this.setState({ pixels: pixels });
            })
            .catch(err => console.log("err", err))
    }



    render()
    {


        return (
            <React.Fragment>
                <Container>
                    <Row>
                    </Row>
                    <Row>
                        <Col xs="12" className="pt-5">
                            <Pixels pixels={this.state.pixels} />
                        </Col>
                    </Row>

                    <Row>
                        <Col xs="12" className="pt-5">
                            <Messages messages={this.state.messages} />
                        </Col>
                    </Row>
                </Container>
            </React.Fragment>
        )
    }
}



export default Main;